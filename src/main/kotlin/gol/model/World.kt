package gol.model

import kotlin.random.Random

typealias CellMatrix = Array<BooleanArray>

class World {
    var afterChange: () -> Unit = {}
    private val rows = 1000
    private val cols = 1000

    var cells : CellMatrix = Array(rows) { BooleanArray(cols) }

    fun clear() {
        cells = Array(rows) { BooleanArray(cols) }
        afterChange()
    }

    fun randomize() {
        cells = Array(rows) { BooleanArray(cols) { Random.nextBoolean() }}
        afterChange()
    }

    fun insertPattern(startingRow: Int, startingCol: Int, pattern: CellMatrix) {
        pattern.forEachCell { row, col, alive ->
            cells[startingRow+row][startingCol+col] = alive
        }
        afterChange()
    }

    fun tick() {
        val nextGeneration : CellMatrix = Array(rows) { BooleanArray(cols) }
        forEachCell { row, col, alive ->
            val aliveNeighbours = aliveNeighbours(row, col)
            nextGeneration[row][col] = (aliveNeighbours==3) || (alive && aliveNeighbours==2)
        }
        cells = nextGeneration
        afterChange()
    }

    fun forEachCell(block: (row: Int, col: Int, alive: Boolean) -> Unit) {
        cells.forEachCell(block)
    }

    private fun aliveNeighbours(row: Int, col: Int) : Int {
        var aliveNeighbours = 0
        if(row>0      && col>0      && isAlive(row-1, col-1)) aliveNeighbours++
        if(row>0                    && isAlive(row-1, col))       aliveNeighbours++
        if(row>0      && col<cols-1 && isAlive(row-1, col+1)) aliveNeighbours++
        if(              col>0      && isAlive(row, col-1))        aliveNeighbours++
        if(              col<cols-1 && isAlive(row, col+1))        aliveNeighbours++
        if(row<rows-1 && col>0      && isAlive(row+1, col-1)) aliveNeighbours++
        if(row<rows-1               && isAlive(row+1, col))       aliveNeighbours++
        if(row<rows-1 && col<cols-1 && isAlive(row+1, col+1)) aliveNeighbours++

        return aliveNeighbours
    }

    private fun isAlive(row: Int, col: Int) : Boolean = cells[row][col]
}

fun CellMatrix.forEachCell(block: (row: Int, col: Int, alive: Boolean) -> Unit) {
    forEachIndexed { row, cellsInThisRow ->
        cellsInThisRow.forEachIndexed { col, alive ->
            block(row, col, alive)
        }
    }
}