package gol.view

import gol.loader.NamedPattern
import gol.model.World
import javafx.beans.property.IntegerProperty
import javafx.scene.canvas.Canvas
import javafx.scene.input.MouseEvent
import javafx.scene.layout.Region
import javafx.scene.paint.Color

class WorldView(
    private val world: World,
    private val cellSize : IntegerProperty,
    private val pattern: () -> NamedPattern?) : Region() {

    private val canvas = Canvas()

    init {
        world.afterChange = this::repaint
        canvas.widthProperty().bind(widthProperty())
        canvas.heightProperty().bind(heightProperty())
        children.setAll(canvas)
        canvas.setOnMouseClicked { mouse ->
            pattern()?.let {
                world.insertPattern(cellRow(mouse), cellCol(mouse), it.pattern)
            }
        }
        cellSize.addListener { _, _, _ -> repaint() }
    }

    fun repaint() = with(canvas.graphicsContext2D) {
        fill = Color.BLACK
        fillRect(0.0,0.0, width, height)

        fill = Color.LIGHTGREEN
        world.forEachCell { row, col, alive ->
            if(alive) fillRect(col * cellSize.doubleValue(), row * cellSize.doubleValue(), cellSize.doubleValue(), cellSize.doubleValue())
        }
    }

    private fun cellRow(mouse: MouseEvent) = (mouse.y / cellSize.doubleValue()).toInt()
    private fun cellCol(mouse: MouseEvent) = (mouse.x / cellSize.doubleValue()).toInt()
}