package gol.view

import gol.loader.loadPatterns
import gol.model.World
import javafx.application.Application
import javafx.application.Platform
import javafx.beans.property.SimpleIntegerProperty
import javafx.scene.Scene
import javafx.stage.Stage

class StageBuilder : Application() {
    override fun start(stage: Stage) = with(stage) {

        val world = World()
        val cellSize = SimpleIntegerProperty(10)
        val controlView = ControlView(world, cellSize)
        val worldView = WorldView(world, cellSize, controlView::selectedPattern)

        title = "Game Of Life"
        scene = Scene(borderPane {
            right = controlView.build()
            center = worldView
        })
        isMaximized = true
        show()
        worldView.repaint()

        Thread {
            loadPatterns("conway-patterns") {
                Platform.runLater { controlView.patterns.add(it) }
            }
        }.start()
    }
}