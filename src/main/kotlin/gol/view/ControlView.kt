package gol.view

import gol.loader.ALIVE_CELL
import gol.loader.DEAD_CELL
import gol.loader.NamedPattern
import gol.model.World
import javafx.animation.AnimationTimer
import javafx.beans.binding.Bindings
import javafx.beans.property.IntegerProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.transformation.FilteredList
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.layout.VBox
import java.util.function.Predicate

class ControlView(
    private val world: World,
    private val cellSize: IntegerProperty
) {

    val patterns = FXCollections.observableArrayList(ALIVE_CELL, DEAD_CELL)
    val searchProp = SimpleStringProperty("")
    val filtered = FilteredList(patterns).apply {
        predicateProperty().bind(
            Bindings.createObjectBinding(
                { Predicate { it.name.contains(searchProp.value) } },
                searchProp
            )
        )
    }
    private val patternList = ListView(filtered).apply { selectionModel.selectionMode = SelectionMode.SINGLE }

    private val timer = animationTimer { world.tick() }

    fun build() : Node = VBox(
        8.0,
        hbox(
            Pos.CENTER,
            Button("Clear").onClick { world.clear() },
            Button("Randomize").onClick { world.randomize() },
        ),
        hbox(
            Pos.CENTER,
            Button("Tick").onClick { world.tick() },
            Button("Start").onClick { timer.start() },
            Button("Stop").onClick { timer.stop() },
        ),
        hbox(
            Pos.CENTER,
            Label("Cell size:"),
            Slider(1.0, 20.0, cellSize.doubleValue()).apply { valueProperty().bindBidirectional(cellSize) },
        ),
        Label("Patterns:"),
        textField(searchProp),
        patternList
    )

    fun selectedPattern() : NamedPattern? = patternList.selectionModel.selectedItem
}

fun animationTimer(onTick: () -> Unit) : AnimationTimer = object: AnimationTimer() {
    override fun handle(now: Long) = onTick()
}