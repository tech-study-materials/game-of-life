package gol.view

import javafx.beans.property.Property
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.scene.control.TextField
import javafx.scene.layout.BorderPane
import javafx.scene.layout.HBox

fun hbox(align: Pos, vararg children: Node) = HBox(8.0, *children).apply {
    alignment = align
}

fun borderPane(block: BorderPane.() -> Unit) = BorderPane().apply(block)

fun Button.onClick(action: () -> Unit) = apply {
    setOnAction { action() }
}


fun textField(prop: Property<String>) =
    TextField().apply { textProperty().bindBidirectional(prop) }


