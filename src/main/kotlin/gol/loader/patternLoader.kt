package gol.loader

import gol.model.CellMatrix
import io.github.robertograham.rleparser.RleParser
import io.github.robertograham.rleparser.domain.PatternData
import org.reflections.Reflections
import org.reflections.scanners.Scanners
import java.util.regex.Pattern

class NamedPattern(val name: String, val pattern: CellMatrix) {
    override fun toString() = name
}

val ALIVE_CELL = NamedPattern("alive cell", Array(1) { BooleanArray(1) {true} })
val DEAD_CELL  = NamedPattern("dead cell", Array(1) { BooleanArray(1) {false} })

fun loadPatterns(location: String, consumer: (NamedPattern) -> Unit) {
    Reflections(location, Scanners.Resources)
        .getResources(Pattern.compile(".*\\.rle"))
        .filterNotNull()
        .mapNotNull { rle ->
            runCatching { RleParser.readPatternData(rle.asResource().toURI()) }
                .onFailure { println("Could not parse $rle : ${it.message}") }
                .mapCatching { NamedPattern(rle.justName(), toCellMatrix(it)) }
                .onFailure { println("Could not create cell matrix from $rle : ${it.message}") }
                .getOrNull()
        }
        .forEach(consumer)
}

private val resLoader = object{}::class.java
private fun String.asResource() = resLoader.getResource("/$this")!!
private fun String.justName() = substring(lastIndexOf('/') + 1, lastIndexOf('.'))
private fun toCellMatrix(data: PatternData) : CellMatrix {
    if(data.metaData.width > 500 || data.metaData.height > 500)
        throw RuntimeException("Too big")
    return Array(data.metaData.height) { BooleanArray(data.metaData.width) }.apply {
        data.liveCells.coordinates.forEach { this[it.y][it.x] = true }
    }
}
