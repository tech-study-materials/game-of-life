package gol

import gol.view.StageBuilder
import javafx.application.Application

fun main() {
    Application.launch(StageBuilder::class.java)
}

