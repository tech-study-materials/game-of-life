fun String.quine() = println(format('"', '"', '"', this, '"', '"', '"'))
fun main() {
"""
fun String.quine() = println(format('"', '"', '"', this, '"', '"', '"'))
fun main() {
%c%c%c%s%c%c%c.quine()
}
""".quine()
}