plugins {
    kotlin("jvm") version "1.6.10"
    id ("org.openjfx.javafxplugin") version "0.0.10"
}


tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "17"
    }
}

repositories {
    mavenCentral()
}


javafx {
    version = "17.0.1"
    modules("javafx.controls", "javafx.web")
}


dependencies {
//    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.reflections:reflections:0.10.2")
    implementation("io.github.robertograham:rle-parser:1.0.1")
}